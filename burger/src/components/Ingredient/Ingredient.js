import React from 'react';

const Ingredient = props => {
    const getIngredient = () => {
        const names = [];
        for (let i = 0; i < props.count; i++) {
            names.push(props.name)
        }
        return names;
    };
    return (
        getIngredient().map((name, index) => {
            return(
                <div key={index} className={`${name}`}></div>
            )
        })
    )
};

export default Ingredient;